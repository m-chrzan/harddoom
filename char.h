#ifndef CHAR_H
#define CHAR_H

#include <linux/pci.h>

#include "pci.h"
#include "private_data.h"

int new_doomdev(struct pci_dev *dev);
void destroy_doomdev(struct doom_data *doom_data);
int char_init(void);
void char_cleanup(void);

#endif
