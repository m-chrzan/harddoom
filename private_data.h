#ifndef PRIVATE_DATA_H
#define PRIVATE_DATA_H

#include <linux/device.h>
#include <linux/cdev.h>

struct doom_data {
	struct cdev cdev;
	struct device *device;
	struct device *pci_device;
	void __iomem *iomem;
	struct mutex cmd_mutex;
	struct mutex ping_mutex;
	struct semaphore pong_sem;
	struct semaphore pong_async_sem;

	uint16_t cmd_counter;

	// cache of registers we don't want to modify when not neccessary
	uint32_t surf_dst_pt;
	uint32_t surf_src_pt;
	uint32_t texture_pt;
	uint32_t flat_addr;
	uint32_t colors_addr;
	uint32_t trans_addr;
	uint32_t surf_dims_w;
	uint32_t surf_dims_h;
	uint32_t texture_dims_s;
	uint32_t texture_dims_h;
	uint32_t draw_params;
};

struct surface_data {
	struct doom_data *doom_data;

	int surface_size;
	int width;
	int height;
	int pages;
	int total_bytes;

	uint8_t *surface_cpu;
	uint32_t *page_table_cpu;
	dma_addr_t surface_dev;
	dma_addr_t page_table_dev;
};

struct texture_data {
	struct doom_data *doom_data;

	uint32_t size;
	uint16_t height;
	int pages;

	uint8_t *texture_cpu;
	uint32_t *page_table_cpu;
	dma_addr_t texture_dev;
	dma_addr_t page_table_dev;
};

struct flat_data {
	struct doom_data *doom_data;

	uint8_t *flat_cpu;
	dma_addr_t flat_dev;
};

struct colors_data {
	struct doom_data *doom_data;

	uint32_t number;

	uint8_t *colors_cpu;
	dma_addr_t colors_dev;
};

#endif
