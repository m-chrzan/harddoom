#ifndef HARDDOOMDEV_H
#define HARDDOOMDEV_H

#include <linux/pci.h>

#include "doomdev.h"
#include "private_data.h"

void start_dev(struct pci_dev *dev);
void shutdown_dev(struct pci_dev *dev);

uint32_t get_interrupts(void __iomem *iomem);
uint32_t get_enabled_interrupts(void __iomem *iomem);
void deactivate_intr(void __iomem *iomem, uint32_t intr);
void disable_intr(void __iomem *iomem, uint32_t intr);
void ping_sync(struct doom_data *doom_data);

void draw_line(struct surface_data *surface_data, struct doomdev_line *line);
void fill_rect(struct surface_data *surface_data, struct doomdev_fill_rect *rect);
void copy_rect(struct surface_data *dst_data, struct surface_data *src_data,
		struct doomdev_copy_rect *rect);
void draw_column(struct surface_data *surface_data,
		struct texture_data *texture_data, struct doomdev_column *column,
		struct colors_data *colors_data, struct colors_data *trans_data,
		uint8_t flags, uint8_t trans_idx);
void draw_span(struct surface_data *surface_data, struct flat_data *flat_data,
		struct doomdev_span *span);
void draw_background(struct surface_data *surface_data,
		struct flat_data *flat_data);

#endif
