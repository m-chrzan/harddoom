#ifndef UTIL_H
#define UTIL_H

#define ORFAIL(cond, label) \
({ \
	err = (cond); \
	if (err < 0) { \
		goto label; \
	} \
})

#define ORFAIL_PTR(ptr, label) \
({ \
	if (IS_ERR(ptr)) { \
		err = PTR_ERR(ptr); \
		goto label; \
	} \
})

#define ORFAIL_NULL(ptr, error, label) \
({ \
	if (ptr == NULL) { \
		err = error; \
		goto label; \
	} \
})

#endif
