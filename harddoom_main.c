#include <linux/module.h>
#include <linux/kernel.h>

#include "char.h"
#include "pci.h"
#include "util.h"

MODULE_LICENSE("GPL");

int harddoom_init(void)
{
	int err = 0;

	ORFAIL(char_init(), error_char) ;
	ORFAIL(pci_init(), error_pci);

	return 0;

error_pci:
	char_cleanup();
error_char:
	return err;
}

void harddoom_cleanup(void)
{
	pci_cleanup();
	char_cleanup();
}

module_init(harddoom_init);
module_exit(harddoom_cleanup);
