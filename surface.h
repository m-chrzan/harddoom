#ifndef SURFACE_H
#define SURFACE_H

#include "doomdev.h"

int new_surface(struct file *filp, struct doomdev_ioctl_create_surface *params);
int new_colors(struct file *filp, struct doomdev_ioctl_create_colormaps *params);
int new_texture(struct file *filp, struct doomdev_ioctl_create_texture *params);
int new_flat(struct file *filp, struct doomdev_ioctl_create_flat *params);

#endif
