KDIR ?= linux

default:
	$(MAKE) -C $(KDIR) M=$$PWD

clean:
	$(MAKE) -C $(KDIR) M=$$PWD clean

tags: pci.c pci.h char.c char.h harddoom_main.c
	ctags -R .
